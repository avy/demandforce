$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'demandforce'
require 'webmock/rspec'
require 'dotenv'
require 'vcr'

RSpec.configure do |config|
  config.color = true
  config.warnings = true

  config.order = :random
  Kernel.srand config.seed

  Dotenv.load
end

def fixture_path
  File.expand_path("../fixtures", __FILE__)
end

VCR.configure do |config|
  config.cassette_library_dir = "#{fixture_path}/vcr_cassettes"
  config.hook_into :webmock
end


