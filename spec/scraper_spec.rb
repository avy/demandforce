require 'spec_helper'

describe Demandforce::Scraper do
  it 'should create new db' do
    db_name = "#{fixture_path}/new.db"
    File.delete(db_name) if File.exist?(db_name)
    Demandforce::Scraper.new(:db => db_name)
    expect(File.exist?(db_name)).to be true
  end

  it 'should return nil if not found an item' do
    db_name = "#{fixture_path}/new.db"
    File.delete(db_name) if File.exist?(db_name)
    client = Demandforce::Scraper.new(:db => db_name)
    expect(client.find_item('not-found')).to be_nil
  end

  it 'should save an item and open db' do
    db_name = "#{fixture_path}/open.db"
    File.delete(db_name) if File.exist?(db_name)
    client = Demandforce::Scraper.new(:db => db_name)
    expect(File.exist?(db_name)).to be true
    id = 'my_url'
    client.save_item(:url_fragment => id)
    client = Demandforce::Scraper.new(:db => db_name)
    item = client.find_item(id)
    expect(item).to be_a(Hash)
    expect(item[:url_fragment]).to eq(id)
  end
  
  it 'should search for urls' do
    client = Demandforce::Scraper.new()
    VCR.use_cassette('scraper-search-urls') do
      urls = client.search('', :get_urls_only => true)
      expect(urls).to be_a(Array)
      expect(urls.size).to eq(10)
      expect(urls[0]).to eq('/b/rodeo-dental-brownsville-1')
    end
  end

  it 'should search for items and an item should have all fields' do
    db_name = "#{fixture_path}/hvac.db"
    File.delete(db_name) if File.exist?(db_name)
    client = Demandforce::Scraper.new(:db => db_name)
    VCR.use_cassette('scraper-search') do
      items = client.search('hvac')
      expect(items).to be_a(Array)
      expect(items.size).to eq(10)
      expect(items[0]).to be_a(Hash)
      expect(items.none?{|i|i[:url_fragment].blank?}).to be true
      item = client.find_item(items[3][:url_fragment])
      expect(client.fields.none?{|f|item[f.to_sym].blank?})
      expect(item[:business_name]).to eq('Armored Heating & Cooling, Inc.')
      expect(item[:industry]).to eq('HVAC & Plumbing')
      expect(item[:address1]).to eq('4252 N Somerset Drive')
      expect(item[:address2]).to eq('')
      expect(item[:city]).to eq('Martinsville')
      expect(item[:state]).to eq('IN')
      expect(item[:zip]).to eq('46151')
      expect(item[:phone_number]).to eq('(317) 890-1566')
      expect(item[:email]).to eq('jason@armoredhvac.com')
      expect(item[:website]).to eq('')
      expect(item[:contact_name]).to eq('Jason Grider')
    end
  end
  
  it 'should fetch an item' do
    client = Demandforce::Scraper.new()
    VCR.use_cassette('scraper-fetch') do
      url = '/b/levydentalgroup'
      item = client.fetch_item(url)
      expect(item[:url_fragment]).to eq(url)
      expect(item[:business_name]).to eq('Levy Dental Group')
      expect(item[:industry]).to eq('Dental')
      expect(item[:address1]).to eq('3120 Kiln Creek Pkwy')
      expect(item[:address2]).to eq('Suite L')
      expect(item[:city]).to eq('Yorktown')
      expect(item[:state]).to eq('VA')
      expect(item[:zip]).to eq('23693')
      expect(item[:phone_number]).to eq('(757) 877-9281')
      expect(item[:email]).to eq('admin@levydentalgroup.com')
      expect(item[:website]).to eq('http://www.levydentalgroup.com/')
      expect(item[:contact_name]).to eq('Guy Levy')
    end
  end
end
