require "active_support/all"
require "faraday"
require "sqlite3"
require "cgi"

module Demandforce

  class Scraper

    def fields
      %w{url_fragment business_name industry address1 address2 city state zip phone_number email website contact_name}
    end

    def initialize(options = {})
      if options[:db].blank?
        @db = nil
      else
        @db = SQLite3::Database.open(options[:db]) rescue SQLite3::Database.new(options[:db])
        columns = fields.map{|f|f + " text#{" primary key" if f == fields[0]}"}
        @db.execute("CREATE TABLE IF NOT EXISTS leads(#{columns.join(', ')})")
      end
    end

    def search_url(query, options)
      page = options[:page].to_i
      page = 1 if page < 1
      params = {:q => query, :p => page, :lc => "0,0", :ls => "360,360", :lz => 1}
      '/search?' + params.to_query
    end
    
    def search(query = '', options = {})
      response = connection.get(search_url(query, options))
      urls = parse_search(response.body).flatten
      return urls if options[:get_urls_only]
      fetch_items(urls, options[:rewrite])
    end

    def fetch_items(urls, rewrite = false)
      items = Array.new 
      urls.each do |url|
        if rewrite or not find_item(url)
          item = fetch_item(url)
          save_item(item)
          items << item
        end
      end
      items
    end

    def fetch_item(url)
      response = connection.get(url)
      parse_item(url, response.body)
    end

    def parse_search(body)
      body.scan(/<a class="bname" href="([^"]+)"/)
    end

    def get_item_prop(body, json, name, reg = nil) 
      reg = /itemprop="#{name}">([^<]+)/ unless reg
      r = (json[name] or (body =~ reg and $1))
      CGI.unescapeHTML(r.to_s.strip)
    end

    def parse_item(id, body)
      data = body.scan(%r{<script type="application/ld\+json">([^<]*)})
      item = {fields[0].to_sym => id}
      json = JSON.parse(data[0][0]) rescue {}
      item[:business_name] = get_item_prop(body, json, 'name',
                              /<meta property="og:title" content="([^"]+)/)
      item[:industry] = get_item_prop(body, json, 'industry', 
                              /<meta name="business:vertical" content="([^"]+)/)
      item[:email] = get_item_prop(body, json, 'email',
                              /<p itemprop="email"[^<]+<[^>]+>([^<]+)/)
      item[:website] = get_item_prop(body, json, 'url',
                              /<p itemprop="url"[^<]+<[^>]+>([^<]+)/)
      item[:phone_number] = get_item_prop(body, json, 'telephone')
      addr = (json['address'] or {})
      item[:address1] = get_item_prop(body, addr, 'streetAddress')
      lines = item[:address1].split(',')
      if lines.size > 1
        item[:address1] = lines[0]
        item[:address2] = lines[1..-1].join(',').strip
      else
        item[:address2] = ''
      end
      item[:city] = get_item_prop(body, addr, 'addressLocality')
      item[:state] = get_item_prop(body, addr, 'addressRegion')
      item[:zip] = get_item_prop(body, addr, 'postalCode')
      item[:contact_name] = get_item_prop(body, json, 'contact_name', 
                              /<section class="business-profile-staff-member">[^>]+>([^<]+)/)
      item
    end

    def find_item(url)
      if @db
        row = @db.get_first_row("SELECT #{fields.join(',')} FROM leads 
                                WHERE #{fields[0]} = ?", url)
        Hash[fields.map(&:to_sym).zip(row)] if row
      end
    end

    def save_item(item)
      return unless @db
      stm = @db.prepare("INSERT OR REPLACE INTO leads(#{fields.join(',')})
                        VALUES(#{"?," * (fields.size - 1)}?)")
      fields.each_with_index do |f, i|
        stm.bind_param(i + 1, item[f.to_sym]) 
      end
      stm.execute
      stm.close
    end

    private

    def connection
      @connection ||= Faraday.new(:url => 'https://local.intuit.com') do |faraday|
        faraday.adapter  Faraday.default_adapter
      end
    end
  end
end
